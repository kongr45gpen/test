#include "Packet.hpp"
#include <cstring>
#include <iostream>

int main() {
    Packet packet = Packet();
    int16_t random_variable = 28;
    uint16_t warum[100] = { 15 };

    packet.appendString(5, "hello");
    packet.appendBits(15, 0x28a8);
    packet.appendBits(1, 3);
    packet.appendFloat(5.7);
    packet.appendString(warum[95], "string");

    std::cout << "Hello, World!" << std::endl;
    std::cout << std::hex << packet.data << std::endl; // packet data must be 'helloQQ'
//    std::cout << *((float*) (packet.data + 7)) << std::endl;
    std::cout  << random_variable << "\n";
    return 0;
}
